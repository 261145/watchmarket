from django.db import models

# Create your models here.
from profiles.models import UserProfileModel
from watches.models import WatchSaleAnnouncementModel


class WishListItemModel(models.Model):
    wishlist_watch = models.ForeignKey(WatchSaleAnnouncementModel,
                                       related_name='wishlist_watch',
                                       on_delete=models.PROTECT)
    wishlist_owner = models.ForeignKey(UserProfileModel,
                                       related_name='wishlist_owner',
                                       on_delete=models.PROTECT)

    create_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Wishlist item'
        verbose_name_plural = 'Wishlist items'
