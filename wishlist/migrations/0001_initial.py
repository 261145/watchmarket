# Generated by Django 3.2.4 on 2021-07-16 13:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('watches', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WishListItemModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(auto_now=True)),
                ('wishlist_owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='wishlist_owner', to=settings.AUTH_USER_MODEL)),
                ('wishlist_watch', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='wishlist_watch', to='watches.watchsaleannouncementmodel')),
            ],
        ),
    ]
