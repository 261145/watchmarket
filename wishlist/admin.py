from django.contrib import admin

# Register your models here.
from wishlist.models import WishListItemModel


class WishListItemModelAdmin(admin.ModelAdmin):
    list_display = ('wishlist_watch', 'wishlist_owner')
    list_filter = ('wishlist_owner', )


admin.site.register(WishListItemModel, WishListItemModelAdmin)
