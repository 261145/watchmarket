# WatchMarket
Questo è un piccolo portale di e-commerce realizzato utilizzando il framework python Django,
presentato come progetto per l'esame del corso di Tecnologie Web ad Unimore.


### Tecnologie utilizzate
* Python 3.7.3
* Django 3.2.4
* django-crispy-forms 1.12.0
* Pillow 8.2.0


## Installazione/Esecuzione (GNU Linux/Mac)
* (Opzionale) È consigliato utilizzare un ambiente virtuale per Python 3 di fiducia
* Aprire un terminale ed entrare nella cartella del progetto
* Eseguire ```pip install -r requirements.txt``` per installare i pacchetti necessari
* Creare il database con ```touch db.sqlite3```
* Lanciare ```python manage.py migrate``` per creare tutte le tabelle del database
* Eseguire ```python manage.py runserver``` per lanciare il server
* Aprire un browser a piacimento e scrivere nella barra di ricerca ```127.0.0.1:8000```

## Installazione/Esecuzione (Windows)
* (Opzionale) È consigliato utilizzare un ambiente virtuale per Python 3 di fiducia
* Aprire un terminale ed entrare nella cartella del progetto
* Eseguire ```pip install -r .\requirements.txt``` per installare i pacchetti necessari
* Creare il database con ```fsutil file createnew db.sqlite3 0```
* Lanciare ```python .\manage.py migrate``` per creare tutte le tabelle del database
* Eseguire ```python .\manage.py runserver``` per lanciare il server
* Aprire un browser a piacimento e scrivere nella barra di ricerca ```127.0.0.1:8000```
