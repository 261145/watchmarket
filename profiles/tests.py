from django.test import Client, TestCase

# Create your tests here.
from django.urls import reverse_lazy

from profiles.models import UserProfileModel


class ProfilesTest(TestCase):
    # setup dove creo un nuovo utente nel db
    def setUp(self):
        self.created_user = UserProfileModel.objects.create_user('provatest',
                                                                 email='prova@prova.it',
                                                                 password='tentativo')

    def test_registration(self):
        client = Client()

        response = client.post(reverse_lazy('profiles:user-registration'),
                               {
                                   'email': 'prova@prova.it',
                                   'username': 'provaregistration',
                                   'password1': 'tentativo',
                                   'password2': 'tentativo'
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        response = client.login(username='provaregistration', password='tentativo')

        self.assertTrue(response)

    # vedere se riesco a loggare un utente creato
    def test_login_successful(self):
        client = Client()
        response = client.login(username='provatest', password='tentativo')

        self.assertTrue(response)

    # vedere se riesco a non loggare un utente fasullo
    def test_login_unsuccessful(self):
        client = Client()
        response = client.login(username='non', password='esiste')

        self.assertFalse(response)

    # test login da metodo post
    def test_login_page_successful(self):
        client = Client()

        response = client.post(reverse_lazy('profiles:user-login'),
                               {
                                   'username': 'provatest',
                                   'password': 'tentativo'
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        client = Client()

        response = client.post(reverse_lazy('profiles:user-login'),
                               {
                                   'username': 'provatest',
                                   'password': 'tentativo'
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        response = client.post(reverse_lazy('profiles:user-logout'),
                               follow=True)

        self.assertEqual(response.status_code, 200)

    # test aggiornamento di un utente
    def test_update_user(self):
        client = Client()

        response = client.login(username='provatest', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('profiles:user-update', kwargs={'pk': self.created_user.pk}),
                               {
                                   'username': 'provaupdatetest',
                                   'email': 'prova@prova.it'
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        # così l'aggiornamento ha effetto
        self.created_user.refresh_from_db()

        self.assertEqual(self.created_user.username, 'provaupdatetest')

    def test_change_password(self):
        client = Client()

        response = client.login(username='provatest', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('profiles:change-password'),
                               {
                                   'old_password': 'tentativo',
                                   'new_password1': 'tentativo2',
                                   'new_password2': 'tentativo2',
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

    def test_change_password_and_login(self):
        client = Client()

        response = client.login(username='provatest', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('profiles:change-password'),
                               {
                                   'old_password': 'tentativo',
                                   'new_password1': 'tentativo2',
                                   'new_password2': 'tentativo2',
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        client.logout()

        response = client.login(username='provatest', password='tentativo2')

        self.assertTrue(response)

    def test_my_user_detail_view(self):
        client = Client()

        response = client.login(username='provatest', password='tentativo')

        self.assertTrue(response)

        response = client.get(reverse_lazy('profiles:user-detail', kwargs={'pk': self.created_user.pk}))

        # così controllo di essere nella mia pagina personale
        self.assertContains(response, 'Edit')

    # controllo che se l'utente non è loggato non veda "edit"
    def test_user_detail_view_not_logged(self):
        client = Client()

        response = client.get(reverse_lazy('profiles:user-detail', kwargs={'pk': self.created_user.pk}))

        # così controllo di essere nella mia pagina personale
        self.assertNotContains(response, 'Edit')
