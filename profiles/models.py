from django.db import models
from django.contrib.auth.models import User, AbstractUser

# https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#custom-users-and-the-built-in-auth-forms
from django.db.transaction import on_commit


class UserProfileModel(AbstractUser):
    email = models.EmailField(verbose_name='Email')
    address = models.CharField(max_length=100, blank=True)
    is_seller = models.BooleanField(default=False,
                                    verbose_name="Are you a seller ?")
    user_photo = models.ImageField(upload_to='profiles/photos',
                                   verbose_name='Profile pic',
                                   blank=True)

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
