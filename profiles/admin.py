from django.contrib import admin

# Register your models here.
from profiles.models import UserProfileModel


class UserProfileModelAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'username', 'email', 'is_seller', 'address', 'date_joined')
    list_filter = ('first_name', 'last_name', 'username', 'is_seller', )
    search_fields = ('first_name__contains', 'last_name__contains', 'username__contains', 'address__contains')
    exclude = ('last_login', 'groups', 'is_active', 'date_joined', 'user_permissions', 'is_superuser')


admin.site.register(UserProfileModel, UserProfileModelAdmin)
