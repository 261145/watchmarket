from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth.forms import UserCreationForm

from profiles.models import UserProfileModel


class UserProfileForm(UserCreationForm):
    helper = FormHelper()
    helper.form_id = 'user_profile_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = UserProfileModel
        fields = ['first_name',
                  'last_name',
                  'address',
                  'email',
                  'username',
                  'is_seller',
                  'user_photo']


# faccio modificare tutto tranne se è venditore
class UserUpdateForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'user_profile_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = UserProfileModel
        fields = ['first_name',
                  'last_name',
                  'address',
                  'email',
                  'username',
                  'user_photo']
