from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DetailView

from profiles.forms import UserProfileForm, UserUpdateForm
from profiles.mixins import IsMyselfMixin
from profiles.models import UserProfileModel


# view per creare un nuovo utente
class UserCreationView(SuccessMessageMixin, CreateView):
    model = UserProfileModel
    form_class = UserProfileForm
    template_name = 'profiles/CRUD/create_user.html'
    success_message = 'User created correctly!'
    success_url = reverse_lazy('profiles:user-login')


# view per aggiornare un utente, controllando che sia chi fa la richiesta a poterlo fare
class UserUpdateView(SuccessMessageMixin, LoginRequiredMixin, IsMyselfMixin, UpdateView):
    model = UserProfileModel
    form_class = UserUpdateForm
    template_name = 'profiles/CRUD/update_user.html'
    success_message = 'User updated correctly!'

    def get_success_url(self):
        return reverse_lazy('profiles:user-detail', kwargs={'pk': self.kwargs['pk']})


# un utente non può eliminarsi da solo

# view per poter visualizzare tutti i dettagli di un certo utente
class UserDetailView(DetailView):
    model = UserProfileModel
    template_name = 'profiles/CRUD/user_detail.html'
