from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from watches.models import WatchSaleAnnouncementModel


class WatchSaleAnnouncementForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'watch_sale_announcement_crispy_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = WatchSaleAnnouncementModel
        fields = ['watch_brand',
                  'watch_model',
                  'watch_reference',
                  'watch_price',
                  'watch_description',
                  'watch_photos',
                  'watch_year',
                  'watch_condition',
                  'watch_size',
                  'watch_case_material',
                  'watch_bracelet_material']
