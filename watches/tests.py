from django.core import mail
from django.test import Client, TestCase

# Create your tests here.
from django.urls import reverse_lazy

from profiles.models import UserProfileModel
from watches.models import WatchSaleAnnouncementModel


class WatchesTest(TestCase):
    def setUp(self):
        self.image_path = 'watches/photos/rolex.jpg'
        self.created_seller = UserProfileModel.objects.create_user('provaseller',
                                                                   email='prova@prova.it',
                                                                   password='tentativo',
                                                                   is_seller=True)
        self.created_seller.save()

        self.created_seller2 = UserProfileModel.objects.create_user('provaseller2',
                                                                    email='prova@prova.it',
                                                                    password='tentativo',
                                                                    is_seller=True)
        self.created_seller2.save()

        self.created_user = UserProfileModel.objects.create_user('provauser',
                                                                 email='prova@prova.it',
                                                                 password='tentativo')
        self.created_user.save()

        self.watch = WatchSaleAnnouncementModel.objects.create(watch_brand='Rolex',
                                                               watch_model='Datejust',
                                                               watch_price=5000.0,
                                                               watch_photos=self.image_path,
                                                               watch_year=2018,
                                                               watch_condition='VERY_GOOD',
                                                               watch_size=39,
                                                               watch_case_material='STAINLESS_STEEL',
                                                               watch_bracelet_material='STAINLESS_STEEL',
                                                               watch_seller=self.created_seller)
        self.watch.save()

        self.watch2 = WatchSaleAnnouncementModel.objects.create(watch_brand='Rolex',
                                                                watch_model='DayDate',
                                                                watch_price=25000.0,
                                                                watch_photos=self.image_path,
                                                                watch_year=2015,
                                                                watch_condition='VERY_GOOD',
                                                                watch_size=36,
                                                                watch_case_material='STAINLESS_STEEL',
                                                                watch_bracelet_material='STAINLESS_STEEL',
                                                                watch_seller=self.created_seller2)
        self.watch2.save()

    # controllo che un utente loggato sia venditore e quindi possa vendere
    def test_logged_user_is_seller(self):
        client = Client()
        response = client.login(username='provaseller', password='tentativo')

        self.assertTrue(response)

        response = client.get(reverse_lazy('watches:insert_sale_announcement'))

        self.assertTrue(response.status_code == 200)

    # controllo che un utente non loggato sia reindirizzato al login
    def test_not_logged_user_sell(self):
        client = Client()

        response = client.get(reverse_lazy('watches:insert_sale_announcement'))

        self.assertTrue(response.status_code == 302)

    # controllo che un non venditore non possa entrare
    def test_logged_user_not_seller(self):
        client = Client()
        response = client.login(username='provauser', password='tentativo')

        self.assertTrue(response)

        response = client.get(reverse_lazy('watches:insert_sale_announcement'))

        self.assertTrue(response.status_code == 403)

    # prova di inserimento annuncio
    def test_insert_watch_announcement(self):
        client = Client()
        response = client.login(username='provaseller', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('watches:insert_sale_announcement'),
                               {
                                   'watch_brand': 'Rolex',
                                   'watch_model': 'DayDate',
                                   'watch_price': 25000,
                                   'watch_photos': '',
                                   'watch_year': 2018,
                                   'watch_condition': 'VERY_GOOD',
                                   'watch_size': 39,
                                   'watch_case_material': 'STAINLESS_STEEL',
                                   'watch_bracelet_material': 'STAINLESS_STEEL'
                               },
                               follow=True)

        self.assertTrue(response.status_code == 200)

    # prova di modifica annuncio
    def test_update_watch_announcement(self):
        client = Client()
        response = client.login(username='provaseller', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('watches:sale_announcement_update', kwargs={'pk': self.watch.pk}),
                               {
                                   'watch_brand': 'Rolex',
                                   'watch_model': 'Datejust',
                                   'watch_price': 6000.0,
                                   'watch_photos': self.image_path,
                                   'watch_year': 2018,
                                   'watch_condition': 'VERY_GOOD',
                                   'watch_size': 39,
                                   'watch_case_material': 'STAINLESS_STEEL',
                                   'watch_bracelet_material': 'STAINLESS_STEEL',
                                   'watch_seller': self.created_seller
                               },
                               follow=True)

        self.assertTrue(response.status_code == 200)

        self.watch.refresh_from_db()

        self.assertEqual(self.watch.watch_price, 6000.0)

    def test_delete_watch(self):
        client = Client()

        response = client.login(username='provaseller', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('watches:sale_announcement_delete', kwargs={'pk': self.watch.pk}),
                               {
                                   'submit': 'Confirm'
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        # se il codice non da errore allora proseguo e fallisco, altrimenti è passato
        try:
            response = client.get(reverse_lazy('watches:sale_announcement_detail', kwargs={'pk': self.watch.pk}))
            self.fail('No exception has been raised')
        except WatchSaleAnnouncementModel.DoesNotExist:
            pass

    def test_no_parameter_search(self):
        client = Client()
        response = client.get(reverse_lazy('watches:sale_announcement_search_list'),
                              {
                                  'order-by': 'p-asc'
                              })

        self.assertEqual(len(response.context['object_list']), 2)

    def test_parameter_search(self):
        client = Client()
        response = client.get(reverse_lazy('watches:sale_announcement_search_list'),
                              {
                                  'watch_brand': 'rolex',
                                  'order-by': 'p-asc'
                              })

        self.assertContains(response, 'rolex', count=2)

    def test_my_watches_view(self):
        client = Client()

        response = client.login(username='provaseller', password='tentativo')
        self.assertTrue(response)

        response = client.get(reverse_lazy('watches:my_sale_announcement_list'))

        # so che l'utente loggato ha un solo orologio
        self.assertEqual(len(response.context['object_list']), 1)

    def test_personal_watches_not_logged(self):
        client = Client()
        response = client.get(reverse_lazy('watches:my_sale_announcement_list'), follow=True)

        # redirezione al login
        self.assertEqual(response.status_code, 200)

    def test_successful_purchase(self):
        client = Client()
        response = client.login(username='provaseller', password='tentativo')
        self.assertTrue(response)

        # prova acquisto mio orologio, dovrebbe fallire
        response = client.post(reverse_lazy('watches:watch_bought', kwargs={'pk': self.watch2.pk}),
                               {
                                   'submit': 'Confirm'
                               }
                               )

        self.assertEqual(response.status_code, 200)

        response = client.get(reverse_lazy('watches:sale_announcement_detail', kwargs={'pk': self.watch2.pk}))

        self.assertContains(response, 'Watch has already been sold!')

        self.assertTrue(WatchSaleAnnouncementModel.objects.get(pk=self.watch2.pk).is_bought)

    def test_purchase_email(self):
        client = Client()
        response = client.login(username='provaseller', password='tentativo')
        self.assertTrue(response)

        # prova acquisto mio orologio, dovrebbe fallire
        response = client.post(reverse_lazy('watches:watch_bought', kwargs={'pk': self.watch2.pk}),
                               {
                                   'submit': 'Confirm'
                               }
                               )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 1)

    def test_failing_purchase(self):
        client = Client()
        client.logout()
        response = client.post(reverse_lazy('watches:watch_bought', kwargs={'pk': self.watch.pk}),
                               {
                                   'submit': 'Confirm'
                               }, follow=True
                               )

        self.assertContains(response, 'login')

        response = client.login(username='provaseller', password='tentativo')
        self.assertTrue(response)

        # prova acquisto mio orologio, dovrebbe fallire
        response = client.post(reverse_lazy('watches:watch_bought', kwargs={'pk': self.watch.pk}),
                               {
                                   'submit': 'Confirm'
                               }
                               )

        self.assertEqual(response.status_code, 403)
