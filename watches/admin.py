from django.contrib import admin

# Register your models here.
from watches.models import WatchSaleAnnouncementModel


class WatchSaleAnnouncementModelAdmin(admin.ModelAdmin):
    list_display = ('watch_brand', 'watch_model', 'watch_reference', 'watch_price', 'watch_year', 'watch_case_material', 'watch_bracelet_material', 'watch_seller')
    list_filter = ('watch_brand', 'watch_model', 'watch_reference', 'watch_year', 'watch_case_material', 'watch_bracelet_material', 'watch_seller')
    search_fields = ('watch_brand__contains', 'watch_model__contains', 'watch_reference__contains')
    exclude = ('is_bought',)


admin.site.register(WatchSaleAnnouncementModel, WatchSaleAnnouncementModelAdmin)
