from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView

from django.db.models import Q, Max

from WatchMarket import settings
from profiles.models import UserProfileModel
from watches.mixins import IsSellerMixin, IsOwnerMixin, IsNotOwnerMixin

from watches.forms import WatchSaleAnnouncementForm
from watches.models import WatchSaleAnnouncementModel


# view utilizzata per l'aggiunta di un nuovo annuncio sul sito
class WatchSaleAnnouncementView(SuccessMessageMixin, LoginRequiredMixin, IsSellerMixin, CreateView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/CRUD/createSaleAnnouncement.html'
    form_class = WatchSaleAnnouncementForm
    success_message = 'Watch inserted correctly!'

    def form_valid(self, form):
        form.instance.watch_seller = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('watches:sale_announcement_detail', kwargs={'pk': self.object.pk})


# view utilizzata per l'aggionamento di un annuncio già inserito, controllo che solo chi è loggato, venditore e
# proprietario possa modificare l'oggetto
class WatchSaleAnnouncementUpdate(SuccessMessageMixin, LoginRequiredMixin, IsSellerMixin, IsOwnerMixin, UpdateView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/CRUD/updateSaleAnnouncement.html'
    form_class = WatchSaleAnnouncementForm
    success_message = 'Watch edited correctly!'

    def get_success_url(self):
        return reverse_lazy('watches:sale_announcement_detail', kwargs={'pk': self.kwargs['pk']})


# view per la rimozione di un annuncio,  controllo che solo chi è loggato, venditore e proprietario
# possa modificare l'oggetto
class WatchSaleAnnouncementDelete(LoginRequiredMixin, IsSellerMixin, IsOwnerMixin, DeleteView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/CRUD/deleteSaleAnnouncement.html'
    success_url = reverse_lazy('watches:my_sale_announcement_list')
    success_message = 'Watch deleted correctly!'

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super(WatchSaleAnnouncementDelete, self).delete(request, *args, **kwargs)


# creo una view dove posso far vedere tutti gli oggetti simili a quello che si sta visualizzando.
# nel queryset ottengo i simili, nel context carico staticamente un oggetto da visualizzare
class WatchSalePostView(ListView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/CRUD/announcementDetail.html'
    WATCH_PRICE_DELTA = 5000

    # l'oggetto di cui far vedere il dettaglio è passato alla pagina da qui
    def get_context_data(self, *args, object_list=None, **kwargs):
        context = super(WatchSalePostView, self).get_context_data(*args, **kwargs)

        context['object_detail'] = WatchSaleAnnouncementModel.objects.get(pk=self.kwargs['pk'])

        return context

    # qui invece trovo tutti gli oggetti simili
    def get_queryset(self):
        watch = WatchSaleAnnouncementModel.objects.get(pk=self.kwargs['pk'])

        # queryset di base con tutti gli oggetti che non sono stati venduti
        base_queryset = WatchSaleAnnouncementModel.objects.filter(is_bought=False)

        base_queryset = base_queryset.exclude(pk=watch.pk)

        queryset_only_brand = base_queryset.filter(watch_brand=watch.watch_brand)
        queryset_brand_and_model = base_queryset.filter(watch_brand=watch.watch_brand,
                                                        watch_model=watch.watch_model)
        queryset_price_range = base_queryset.filter(watch_price__range=(watch.watch_price - self.WATCH_PRICE_DELTA,
                                                                        watch.watch_price + self.WATCH_PRICE_DELTA))

        queryset_size = base_queryset.filter(watch_size=watch.watch_size)

        queryset_bracelet_material = base_queryset.filter(watch_bracelet_material=watch.watch_bracelet_material)

        queryset_case_material = base_queryset.filter(watch_case_material=watch.watch_case_material)

        # ritorno la combinazione di tutti gli orologi cercati, senza duplicati di default (altrimenti all=True)
        # i parametri comprendono:
        # - stesso il brand
        # - stesso il brand e modello
        # - stessa la dimensione della cassa
        # - stessi materiali di costruzione
        queryset = queryset_only_brand.union(queryset_brand_and_model, queryset_size, queryset_bracelet_material,
                                             queryset_case_material)

        # ritorno il queryset con un delta di prezzi fissato
        return queryset.intersection(queryset_price_range)


# view per vedere tutti gli annunci pubblicati
class SaleAnnouncementList(ListView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/watchesList.html'
    paginate_by = 10


# view per vedere gli annunci pubblicati da me (loggato), qui il controllo che altri non possano accedere
# a questa pagina è fatto col queryset, prendendo chi fa la richiesta
class PersonalSaleAnnouncement(LoginRequiredMixin, ListView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/personalList.html'
    paginate_by = 10

    def get_queryset(self):
        return WatchSaleAnnouncementModel.objects.filter(
            watch_seller=self.request.user
        ).order_by('create_date')


# view per vedere gli annunci solo di un certo utente
class ProfileShopView(ListView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/profileShop.html'
    paginate_by = 10

    def get_queryset(self):
        return WatchSaleAnnouncementModel.objects.filter(
            watch_seller_id=self.kwargs['pk']
        )

    # così ottengo il nome dell'utente e posso tornare indietro per migliorare la navigabilità
    def get_context_data(self, *args, **kwargs):
        context = super(ProfileShopView, self).get_context_data(*args, **kwargs)

        context['user_shop'] = UserProfileModel.objects.get(pk=self.kwargs['pk'])

        return context


# view per la ricerca di oggetti per modello, brand e diversi altri parametri
class WatchSearchView(ListView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/watchesList.html'
    paginate_by = 10

    # rimetto i dati nel contesto per ripopolare la ricerca e far funzionare la pagination con
    # la query che c'era prima
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(WatchSearchView, self).get_context_data(**kwargs)

        sorting_type = self.request.GET.get('order-by') or 'p-asc'

        if sorting_type == 'p-asc':
            sorting_type = 'watch_price'
        elif sorting_type == 'p-desc':
            sorting_type = '-watch_price'
        elif sorting_type == 'date-asc':
            sorting_type = 'create_date'
        elif sorting_type == 'date-desc':
            sorting_type = '-create_date'
        elif sorting_type == 'year-asc':
            sorting_type = 'watch_year'
        elif sorting_type == 'year-desc':
            sorting_type = '-watch_year'

        context['query_brand'] = self.request.GET.get('query-brand') or None
        context['query_model'] = self.request.GET.get('query-model') or None
        context['query_reference'] = self.request.GET.get('ref-number') or None
        context['min_price'] = self.request.GET.get('start-price') or 0
        context['max_price'] = self.request.GET.get('end-price') or WatchSaleAnnouncementModel.objects.all() \
            .aggregate(Max('watch_price'))['watch_price__max']
        context['min_year'] = self.request.GET.get('start-year') or 0
        context['max_year'] = self.request.GET.get('end-year') or WatchSaleAnnouncementModel.objects.all() \
            .aggregate(Max('watch_year'))['watch_year__max']
        context['min_case_size'] = self.request.GET.get('start-case-size') or 0
        context['max_case_size'] = self.request.GET.get('end-case-size') or WatchSaleAnnouncementModel.objects.all() \
            .aggregate(Max('watch_size'))['watch_size__max']
        context['case_material'] = self.request.GET.get('case-material') or None
        context['bracelet_material'] = self.request.GET.get('bracelet-material') or None
        context['conditions'] = self.request.GET.get('conditions') or None
        context['sorting_type'] = sorting_type
        context['is_search'] = True  # dico anche che siamo in una pagina di ricerca, per far funzionare il JS

        return context

    def get_queryset(self):
        # ottengo tutti i filtri dalla richiesta in get
        query_brand = self.request.GET.get('query-brand') or None
        query_model = self.request.GET.get('query-model') or None
        query_reference = self.request.GET.get('ref-number') or None

        min_price = self.request.GET.get('start-price') or 0
        max_price = self.request.GET.get('end-price') or WatchSaleAnnouncementModel.objects.all() \
            .aggregate(Max('watch_price'))['watch_price__max']  # ottengo il massimo di tutti gli oggetti nel db

        min_year = self.request.GET.get('start-year') or 0
        max_year = self.request.GET.get('end-year') or WatchSaleAnnouncementModel.objects.all() \
            .aggregate(Max('watch_year'))['watch_year__max']

        min_case_size = self.request.GET.get('start-case-size') or 0
        max_case_size = self.request.GET.get('end-case-size') or WatchSaleAnnouncementModel.objects.all() \
            .aggregate(Max('watch_size'))['watch_size__max']

        case_material = self.request.GET.get('case-material') or None

        bracelet_material = self.request.GET.get('bracelet-material') or None

        conditions = self.request.GET.get('conditions') or None

        # il sort di default è per prezzo ascendente
        sorting_type = self.request.GET.get('order-by') or 'p-asc'

        if sorting_type == 'p-asc':
            sorting_type = 'watch_price'
        elif sorting_type == 'p-desc':
            sorting_type = '-watch_price'
        elif sorting_type == 'date-asc':
            sorting_type = 'create_date'
        elif sorting_type == 'date-desc':
            sorting_type = '-create_date'
        elif sorting_type == 'year-asc':
            sorting_type = 'watch_year'
        elif sorting_type == 'year-desc':
            sorting_type = '-watch_year'

        # controllo i valori nulli nei filtri singoli
        queryset = WatchSaleAnnouncementModel.objects.all()
        if query_brand:
            queryset = queryset.filter(
                Q(watch_brand__icontains=query_brand)
            )
        if query_model:
            queryset = queryset.filter(
                Q(watch_model__icontains=query_model)
            )
        if query_reference:
            queryset = queryset.filter(
                Q(watch_reference__contains=query_reference)
            )
        if case_material:
            queryset = queryset.filter(
                Q(watch_case_material__startswith=case_material)
            )

        if bracelet_material:
            queryset = queryset.filter(
                Q(watch_bracelet_material__startswith=bracelet_material)
            )

        if conditions:
            queryset = queryset.filter(
                Q(watch_condition__startswith=conditions)
            )

        # filtri in range che sono sempre applicabili, quindi non serve un controllo
        return queryset.filter(
            (Q(watch_price__range=(min_price, max_price))
             & Q(watch_year__range=(min_year, max_year))
             & Q(watch_size__range=(min_case_size, max_case_size)))
        ).order_by(sorting_type)


# view visualizzata prima della conferma di acquisto di un orologio, così da far vedere
# un riassunto dell'orologio che si sta per acquistare (controllo che non sia proprietario)
class ConfirmBuyView(LoginRequiredMixin, IsNotOwnerMixin, DetailView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/purchase/confirmation.html'


# view per confermare l'acquisto effettuato dell'orologio, viene inviata una mail e
# l'orologio è segnato come acquistato nel database
class BoughtView(LoginRequiredMixin, IsNotOwnerMixin, DetailView):
    model = WatchSaleAnnouncementModel
    template_name = 'watches/purchase/bought.html'
    context_object_name = 'object'  # nel template potrò accedere all'oggetto che mi viene restituito come context

    # chiamata dopo la conferma dell'acquisto da parte dell'utente tramite una richiesta in post alla pagina
    # alla fine del processo viene visualizzata la pagina di scrittura di una review
    def post(self, request, *args, **kwargs):
        # modifica dello stato dell'oggetto
        obj = WatchSaleAnnouncementModel.objects.get(pk=self.kwargs['pk'])
        obj.is_bought = True
        obj.save()

        # invio mail di conferma
        subject = '[Watch purchase confirmation]'

        message = f'We\'re glad to inform you that your purchase' \
                  f' of the watch {obj.watch_brand} {obj.watch_model}' \
                  f' from {obj.watch_seller.username} has gone well and' \
                  f' it\'s been processed.\n\nYou\'ll soon receive your new watch!'

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [request.user.email])

        # passo il mio oggetto nel contesto della pagina
        return render(request, self.template_name, context={'object': obj})
