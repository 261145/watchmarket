from datetime import date

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

# Create your models here.
import profiles


CONDITION_CHOICES = (
    ('NEW_NEVER_WORN', 'New/Never worn'),
    ('VERY_GOOD', 'Very Good'),
    ('GOOD', 'Good'),
    ('POOR', 'Poor'),
    ('NOT_WORKING', 'Not working')
)

CASE_MATERIAL_CHOICES = (
    ('STAINLESS_STEEL', 'Stainless Steel'),
    ('WHITE_GOLD', 'White Gold'),
    ('YELLOW_GOLD', 'Yellow Gold'),
    ('ROSE_GOLD', 'Rose Gold'),
    ('PLATINUM', 'Platinum'),
    ('TITANIUM', 'Titanium'),
    ('OTHER', 'Other')
)

BRACELET_MATERIAL_CHOICES = (
    ('STAINLESS_STEEL', 'Stainless Steel'),
    ('WHITE_GOLD', 'White Gold'),
    ('YELLOW_GOLD', 'Yellow Gold'),
    ('ROSE_GOLD', 'Rose Gold'),
    ('PLATINUM', 'Platinum'),
    ('TITANIUM', 'Titanium'),
    ('LEATHER', 'Leather'),
    ('RUBBER', 'Rubber'),
    ('OTHER', 'Other')
)


class WatchSaleAnnouncementModel(models.Model):
    watch_brand = models.CharField(max_length=100,
                                   verbose_name='Brand of the watch')
    watch_model = models.CharField(max_length=100,
                                   verbose_name='Model of the watch')
    watch_reference = models.CharField(max_length=50,
                                       verbose_name='Reference number of the watch',
                                       blank=True)
    watch_price = models.FloatField(verbose_name='Price',
                                    validators=[MinValueValidator(1)])
    watch_seller = models.ForeignKey(profiles.models.UserProfileModel,
                                     related_name='watch_seller',
                                     on_delete=models.PROTECT)
    watch_description = models.TextField(verbose_name='Description',
                                         blank=True)
    watch_photos = models.ImageField(upload_to='watches/photos',
                                     verbose_name='Pic of the watch')
    watch_year = models.IntegerField(verbose_name='Year of production',
                                     validators=[MinValueValidator(date.min.year),
                                                 MaxValueValidator(date.today().year)])
    watch_condition = models.CharField(max_length=500,
                                       choices=CONDITION_CHOICES,
                                       verbose_name='Select watch\'s condition')
    watch_size = models.IntegerField(verbose_name='Size of the case (mm)',
                                     validators=[MinValueValidator(1)])
    watch_case_material = models.CharField(choices=CASE_MATERIAL_CHOICES,
                                           verbose_name='Select your watch\'s case material',
                                           max_length=500)
    watch_bracelet_material = models.CharField(choices=BRACELET_MATERIAL_CHOICES,
                                               verbose_name='Select your watch\'s bracelet material',
                                               max_length=500)
    create_date = models.DateTimeField(auto_now=True)
    is_bought = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Watch'
        verbose_name_plural = 'Watches'
