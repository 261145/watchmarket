from django.urls import path

from reviews.views import WriteReviewView, ListReviewsOfSeller, ReviewUpdate, ReviewDelete

app_name = 'reviews'

urlpatterns = [
    # la pk è utilizzata per salvarsi il venditore di cui si sta facendo la review
    path('<int:pk>/write/', WriteReviewView.as_view(), name='leave-review'),
    path('<int:pk>/sellerreviews/', ListReviewsOfSeller.as_view(), name='seller-reviews'),
    path('<int:pk>/update/', ReviewUpdate.as_view(), name='review-update'),
    path('<int:pk>/delete/', ReviewDelete.as_view(), name='review-delete'),

]
