from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

# Create your models here.
from profiles.models import UserProfileModel


class ReviewModel(models.Model):
    review_title = models.CharField(max_length=200,
                                    verbose_name='Title of the review')
    review_content = models.TextField(max_length=1000,
                                      verbose_name='Content of the review')
    review_seller_rating = models.IntegerField(validators=[MaxValueValidator(5),
                                               MinValueValidator(0)],
                                               verbose_name='Rating of the seller')
    review_seller = models.ForeignKey(UserProfileModel,
                                      related_name='review_seller',
                                      on_delete=models.PROTECT)
    review_author = models.ForeignKey(UserProfileModel,
                                      related_name='review_author',
                                      on_delete=models.PROTECT)
    create_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'
