from django.http import HttpResponseForbidden

from profiles.models import UserProfileModel
from reviews.models import ReviewModel


# mixin per controllare che l'utente che fa la richiesta sia l'autore della review
class IsAuthorMixin:
    def dispatch(self, request, *args, **kwargs):
        review = ReviewModel.objects.get(pk=kwargs['pk'])
        if not request.user == review.review_author:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


# mixin per controllare che l'utente di cui si stanno guardando o scrivendo le review sia un venditore
class UserIsSellerMixin:
    def dispatch(self, request, *args, **kwargs):
        user = UserProfileModel.objects.get(pk=kwargs['pk'])
        if not user.is_seller:
            # uso questo perché nelle listview non c'è il metodo handle_no_permission
            return HttpResponseForbidden("<h1>403 Forbidden</h1>")
        return super().dispatch(request, *args, **kwargs)


# mixin per controllare che un utente non possa lasciarsi reviews da solo
class IsNotMyselfMixin:
    def dispatch(self, request, *args, **kwargs):
        affected_user = UserProfileModel.objects.get(pk=kwargs['pk'])
        if request.user == affected_user:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
