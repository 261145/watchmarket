from django.contrib import admin

# Register your models here.
from reviews.models import ReviewModel


class ReviewModelAdmin(admin.ModelAdmin):
    list_display = ('review_title', 'review_seller_rating', 'review_seller', 'review_author')
    list_filter = ('review_title', 'review_seller_rating', 'review_seller', 'review_author')
    search_fields = ('review_title__contains', )


admin.site.register(ReviewModel, ReviewModelAdmin)
