from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Avg
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from profiles.models import UserProfileModel
from reviews.forms import LeaveReviewForm
from reviews.mixins import IsAuthorMixin, UserIsSellerMixin, IsNotMyselfMixin
from reviews.models import ReviewModel


# view per poter lasciare una recensione ad un venditore
class WriteReviewView(SuccessMessageMixin, UserIsSellerMixin, IsNotMyselfMixin, LoginRequiredMixin, CreateView):
    model = ReviewModel
    form_class = LeaveReviewForm
    template_name = 'reviews/CRUD/leave_review.html'
    success_message = 'Review created correctly!'

    def form_valid(self, form):
        form.instance.review_seller = UserProfileModel.objects.get(pk=self.kwargs['pk'])
        form.instance.review_author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.kwargs['pk']})


# view per vedere tutte le recensioni risolve ad un certo venditore, chi non è venditore non ha review
class ListReviewsOfSeller(UserIsSellerMixin, ListView):
    model = ReviewModel
    template_name = 'reviews/seller_reviews.html'
    paginate_by = 10

    def get_queryset(self):
        # ottengo soltanto le review che riguardano il particolare utente che sto guardando ora
        return ReviewModel.objects.filter(
            review_seller_id=self.kwargs['pk']
        ).order_by('create_date')

    def get_context_data(self, *args, **kwargs):
        # ottengo il venditore per poterci scrivere il nome e migliorare la navigabilità
        context = super(ListReviewsOfSeller, self).get_context_data(*args, **kwargs)

        seller = UserProfileModel.objects.get(pk=self.kwargs['pk'])

        context['seller'] = seller

        if ReviewModel.objects.filter(review_seller=seller).count() != 0:
            context['seller_average_rating'] = ReviewModel.objects.filter(review_seller=seller) \
                .aggregate(Avg('review_seller_rating'))['review_seller_rating__avg']
        else:
            context['seller_average_rating'] = -1

        return context


# view per effettuare la modifica di una review da parte di chi l'ha scritta
class ReviewUpdate(SuccessMessageMixin, LoginRequiredMixin, IsAuthorMixin, UpdateView):
    model = ReviewModel
    template_name = 'reviews/CRUD/update_review.html'
    form_class = LeaveReviewForm
    success_message = 'Review updated correctly!'

    def get_success_url(self):
        review = ReviewModel.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('reviews:seller-reviews', kwargs={'pk': review.review_seller.pk})


# view per poter far cancellare la review da parte dell'autore
class ReviewDelete(LoginRequiredMixin, IsAuthorMixin, DeleteView):
    model = ReviewModel
    template_name = 'reviews/CRUD/delete_review.html'
    success_message = 'Review deleted correctly!'

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super(ReviewDelete, self).delete(request, *args, **kwargs)

    def get_success_url(self):
        review = ReviewModel.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('reviews:seller-reviews', kwargs={'pk': review.review_seller.pk})
